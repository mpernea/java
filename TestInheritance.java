public class TestInheritance
{
	public static void main(String[] args)
	{
		Account[] typesOfAccounts= {
								new SavingsAccount("Sarah", 2), // modified to Savings for the optional exercise 
								new SavingsAccount("Alex", 4),
								new CurrentAccount("Abigail", 6)
							};
        System.out.println(typesOfAccounts[1].getName()); // check if array has generated correctly 
        
        for (int i=0; i < typesOfAccounts.length; i++) {
            typesOfAccounts[i].addInterest(); // add interest on each account according to the type of account it is 
            System.out.println("The account name is " + typesOfAccounts[i].getName() + " and the balance with added interest is " + typesOfAccounts[i].getBalance());
        }
	}


}