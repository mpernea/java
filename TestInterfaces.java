public class TestInterfaces {
    public static void main(String[] args) {
        Detailable[] details = {
            new HomeInsurance(2000, 500, 5000),
            new CurrentAccount("Marius", 600),
            new SavingsAccount("Alex", 1500)
        };

        for (int i=0; i<details.length; i++) {
            System.out.println(details[i].getDetails());
        }
    }
    
}