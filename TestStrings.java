import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class TestStrings {
    public static void main(String[] args) {
        
        // Replace example.doc with example.bak
        // String s = "example.doc";
        // String s1 = s.replace("doc","bak");
        // System.out.print("Replaced string is " + s1);

        StringBuilder fileName = new StringBuilder("example.doc");
        fileName.replace(fileName.length()-3, fileName.length(), "bak");
        System.out.println("Replaced string is "+ fileName);

        String s1 = "hello mate";
        String s2 = "hello mate";

        if (s1.equals(s2)) {
            System.out.println("strings match");
        }
        else {
            System.out.println("strings do not match");
        }

        // count the number of ow
        String owString = "the quick brown fox swallowed down the lazy chicken";
        int count =0;
        for (int i=0; i<owString.length()-1; i++) {
            if (owString.substring(i, i+2).equals("ow")) {
                count++;
            }
        }
        System.out.println("The number of repeats is " + count);

        // palindrome strings
        StringBuilder pali = new StringBuilder("Live not on evil");
        StringBuilder paliCopy = new StringBuilder(pali);
        System.out.println(pali.reverse().toString()); // this reverses the string given and prints it
        if (pali.reverse().toString().equalsIgnoreCase(paliCopy.toString())) { // need to convert pali from StringBuilder object to String object
            System.out.println("the string is a palindrome");
        }
        else System.out.println("string is not palindrome");

        // printing out time formats
        Format day_month_year = new SimpleDateFormat("dd mm yyyy");
        Format year_only = new SimpleDateFormat("yyyy");
        Format time_only = new SimpleDateFormat("hh mm");
        System.out.println("Day, month and year is " + day_month_year);
        System.out.println("Year is " + year_only);
        System.out.println("The time today is " + time_only);


       
        


    }
}