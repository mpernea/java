public class TestAccount {
    public static void main(String[] args) {
        Account myAccount = new Account("Marius", 900); // using this structure because I added a constructor in my Account class
        // myAccount.setBalance(900); this is how you would do it if you had no constructor defined
        // myAccount.setName("Marius");
        System.out.print("My name is " + myAccount.getName() + " and my balance is " + myAccount.getBalance());
        
        // now I will add my interest
        myAccount.addInterest(myAccount.getBalance());
        System.out.print("\nMy name is " + myAccount.getName() + " and my balance is " + myAccount.getBalance()); 
        
        // Account[] arrayofAccounts; // declare the array 
        Account[] arrayofAccounts = new Account[5]; // create the array with 5 fields
        
        double[] amounts = {200, 400, 600, 800, 1400};
        String[] names = {"P1", "P2", "P3", "P4", "P5"};

        // for (int i=0; i<arrayofAccounts.length; i++) {
        //     arrayofAccounts[i] = new Account();
        //     arrayofAccounts[i].setName(names[i]);
        //     arrayofAccounts[i].setBalance(amounts[i]);
        //     System.out.println("\nMy name is " + arrayofAccounts[i].getName() + " and my balance is " + arrayofAccounts[i].getBalance());
        //     // adding interest
        //     arrayofAccounts[i].addInterest(arrayofAccounts[i].getBalance());
        //     System.out.println("\nMy name is " + arrayofAccounts[i].getName() + " and my NEW balance is " + arrayofAccounts[i].getBalance());  
        // }
    }
    
}