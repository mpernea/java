package com.conygre.test.cars;

// import java.beans.BeanProperty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration

public class PersonConfiguration {

@Bean
public Car car() {
    return new Audi();
}

@Bean 
public Person person(@Autowired Car car) {
    return new Person(car);
}
}