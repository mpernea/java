package com.conygre.test.cars;

public interface Car {
    void drive();
}