package com.conygre.test.cars;

public class Mercedes implements Car {
    public void drive() {
        System.out.println("You now have a Mercedes");
    }
}