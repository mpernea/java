package com.conygre.test;

import com.conygre.test.cars.Person;
import com.conygre.test.cars.PersonConfiguration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class PersonTestCar {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(PersonConfiguration.class);
        context.getBean(Person.class).getCar().drive();
    }
}