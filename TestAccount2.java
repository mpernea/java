public class TestAccount2 {
    public static void main(String[] args) {

        Account[] arrayofAccounts;
        arrayofAccounts = new Account[5];

        String[] names = {"Name1", "Name2", "Name3", "Name4", "Name5"};
        double[] balance = {2000, 3000, 8000, 9000, 14000};

        for (int i=0; i<arrayofAccounts.length; i++) { 
            arrayofAccounts[i] = new Account(names[i], balance[i]);
            System.out.println("\nMy name is " + arrayofAccounts[i].getName() + " and my balance is " + arrayofAccounts[i].getBalance());
            arrayofAccounts[i].addInterest();
            System.out.println("\nThe new balance is " + arrayofAccounts[i].getBalance());
            // arrayofAccounts[i].withdraw(200);
        }

        arrayofAccounts[0].withdraw(200); // uses the withdraw method specifically saying 200
        arrayofAccounts[1].withdraw(); // uses the withdraw method which calls for withdraw(100)
    }
}