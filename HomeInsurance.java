public class HomeInsurance implements Detailable {

    private double p;
    private double e;
    private double ai;
    
    public HomeInsurance (double p, double e, double ai) {
        this.p = p;
        this.e = e;
        this.ai = ai;
    }

    @Override // not necessarily needed?
    public String getDetails() {
        return  "Your excess is " + e + " and your amount insured is " + ai + " and your premium is " + p;
    }

}