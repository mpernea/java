public class HelloWorld {
    public static void main(String[] args) {
        Car car1 = new Car();
        car1.setMake("Audi");
        
        Car car2 = car1; // so far we only have ONE car object. Car 2 
        // is a copy of car1 but just pointing to the object created for car1.
        // word NEW implies a new object, otherwise it is just a pointer 

        car2.setMake("Renault");
        System.out.println(car1.getMake());

        // there is only one object so changing that one car will change the object 
        // itself, it does not matter which reference to the same object you
        // use

        car1.accelerate();
        System.out.println(car2.getSpeed());
        // accelerating with car1, printing with car2. Printing out will be 1 again 
        // because it points to the same object, there are 2 references but it is only 
        // one object 

        Account acc1 = new Account();
        acc1.setBalance(600);
        acc1.setName("John");
        System.out.println(acc1.getName() + " has this much in his balance - " + acc1.getBalance());
        acc1.addInterest(acc1.getBalance());
        System.out.println(acc1.getName() + " has this much in his balance with interest - " + acc1.getBalance());

        Account acc2 = new Account();
        acc2.setBalance(1200);
        acc2.setName("Alex");
        System.out.print("\nThird name is " + acc2.getName() + " and his balance is " + acc2.getBalance()); 
        acc2.addInterest(acc2.getBalance());
        System.out.print("\nThird name is " + acc2.getName() + " and his balance is " + acc2.getBalance()); 
    }
}