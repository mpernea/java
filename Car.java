public class Car {
    // properties
    private String make;
    private String model;
    private int speed;

    // methods

    // getter for the make
    // public String getMake() {
    //     return make; 
    // }

    // public void setMake(String m) {
    //     make = m;
    // }

    public void accelerate() {
        speed+=1; 
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}