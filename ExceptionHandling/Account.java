public  class Account { 
    private String name;
    private double balance;

    // DEFAULT CONSTRUCTOR
    public Account() {
    }

    // MODIFIED CONSTRUCTOR TO THROW
    public Account(String name) throws DodgyNameException {
        if ("Fingers".equals(name))
            throw new DodgyNameException();
        this.name = name;
    }

    // ADD INTEREST

    public void addInterest() {
        balance *= 1.1;
    }

    // SET AND GET BALANCE
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    // SET AND GET METHOD FOR NAME
    public String getName() {
        return name;
    }

    // public void setName(String name) {
    //     this.name = name;
    // }

    public void setName(String name) throws DodgyNameException {
        if ("Fingers".equals(name))
            throw new DodgyNameException();
        this.name=name;
    }

    public Account(String name, double balance) throws DodgyNameException {
        if ("Fingers".equals(name))
            throw new DodgyNameException();
        this.name = name;
        this.balance = balance;
    }

    
    
}