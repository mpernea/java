public class TestExceptions {
    public static void main(String[] args) {
        Account[] arrayofAccounts;
        arrayofAccounts = new Account[5];

        String[] names = {"Name1", "Name2", "Name3", "Name4", "Name5"};
        double[] balance = {2000, 3000, 8000, 9000, 14000};

        try 
        {
            for (int i=0; i<arrayofAccounts.length; i++) { 
                arrayofAccounts[i] = new Account(names[i], balance[i]);
                System.out.println("\nMy name is " + arrayofAccounts[i].getName() + " and my balance is " + arrayofAccounts[i].getBalance());
                arrayofAccounts[i].addInterest();
                System.out.println("\nThe new balance is " + arrayofAccounts[i].getBalance());
            }
        }
        catch (DodgyNameException e)
        {
            System.out.println("Exception: " + e);
            return;
        }
        
        finally {
            double taxCollected = 0;
            for (int i=0; i<arrayofAccounts.length; i++) {
                taxCollected = taxCollected + 0.4 * arrayofAccounts[i].getBalance();
                arrayofAccounts[i].setBalance(arrayofAccounts[i].getBalance()*0.6);
            }
            System.out.println("Tax collected is " + taxCollected);
        }
    }
}