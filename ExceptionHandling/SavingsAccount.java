public class SavingsAccount extends Account {
    public SavingsAccount (String s, double d) {
        super(s,d);
    }

    // based on an interest rate for savings account
    @Override 
    // to over rule the add interest method found in the Account class (which is now empty)
    public void addInterest() {
        this.setBalance(getBalance()*1.4);
    }
    
}