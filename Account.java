public abstract class Account implements Detailable { // included the abstract word
    private String name;
    private double balance;
    
    private static double interestRate = 1.02;

    // static get and set methods for the interest rate 

    public static void setInterestRate (double d) {
        interestRate = d;
    }

    public static double getInterestRate (double d) {
        return InterestRate;
    }

    // method for using the interest rate

    public abstract void addInterest();
    // {
        // this is now empty as both CurrentAccount and SavingsAccount have their own methods of adding interest
    //}

    // constructor

    public Account(String s, double d) {
        name = s;
        balance = d;
    }

    // public Account() {
    //     name = "Alex";
    //     balance = 800;
    // }

    public String getDetails() {
        return "The name is " + name + " and the balance is " + balance;
    }
    
    // set and  get methods for the balance
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    // set and get method for the name
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // public double addInterest(double balance) {
    //     this.balance = balance * 1.1;
    //     return balance;
    // }

    // method overloading

    public boolean withdraw() {
        return withdraw (100);
    }
    public boolean withdraw(double amount) {
        boolean flag = false;
        if (balance >= amount) {
            flag = true;
            balance = balance - amount;
            System.out.println("You withdrew an amount of " + amount + " and your new balance is " + balance);
        }
        else {
            System.out.println("There is not enough money in your balance");
        }
        return flag;
    }
}