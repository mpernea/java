package chapter14;
import java.util.*;

public class CollectionsTest {
    public static void main(String[] args) {
       HashSet<Account> accounts;
       accounts = new HashSet<Account>();
       accounts.add(new Account("Marius", 600));
       accounts.add(new Account("John", 800));
       accounts.add(new Account("Darren", 1000));
    
       // define the iterator
       Iterator<Account> iter = accounts.iterator();

       // iterate through the hashet
       while (iter.hasNext()) {
            Account nextAccount = iter.next();
            System.out.println("Name is " + nextAccount.getName() + " and balance is " + nextAccount.getBalance());
            nextAccount.addInterest();
            System.out.println("Name is " + nextAccount.getName() + " and balance with interest is " + nextAccount.getBalance());
       }
       // for each method java 5
       System.out.println("Java 5 method");
       for (Account nextAccount: accounts) // loop through the accounts hashset 
       {
            System.out.println("Name is " + nextAccount.getName() + " and balance is " + nextAccount.getBalance());
            nextAccount.addInterest();
            System.out.println("Name is " + nextAccount.getName() + " and balance with interest is " + nextAccount.getBalance());
       }

       // for each method java 8 
       
    }

}