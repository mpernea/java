public class Lambda {
    public static void main(String[] args) {
        List<Pet> petsInAList = new ArrayList<Pet>();
        petsInAList.add(new Cat());

        petsInAList.forEach(System.out::println);
        
        Pet petLambda = () -> System.out.println("feed pet lambda");
        petLambda.feed(); // the feed function in the other file which does System.out.println()
    }
}