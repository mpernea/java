package chapter14;

public class Account { 
    private String name;
    private double balance;

    // method for using the interest rate

    public void addInterest()
    {
        balance = balance * 1.1;
    }

    // constructor
    public Account(String s, double d) {
        name = s;
        balance = d;
    }

    public Account() {

    }

    // set and  get methods for the balance
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    // set and get method for the name
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}